<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="be_BY" sourcelanguage="ru_RU">
<context>
    <name>AboutDialog</name>
    <message>
        <source>KeePassX %1</source>
        <translation type="obsolete">KeePassX %1</translation>
    </message>
    <message>
        <source>&lt;b&gt;Current Translation: None&lt;/b&gt;&lt;br&gt;&lt;br&gt;</source>
        <comment>Please replace &apos;None&apos; with the language of your translation</comment>
        <translation type="obsolete">&lt;b&gt;Current Translation: Russian&lt;/b&gt;&lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;Author:&lt;/b&gt; %1&lt;br&gt;</source>
        <translation type="obsolete">&lt;b&gt;Автор:&lt;/b&gt; %1&lt;br&gt;</translation>
    </message>
    <message>
        <source>$TRANSLATION_AUTHOR</source>
        <translation type="obsolete">Дмитрий Функ</translation>
    </message>
    <message>
        <source>$TRANSLATION_AUTHOR_EMAIL</source>
        <comment>Here you can enter your email or homepage if you want.</comment>
        <translation type="obsolete">dmitry.funk@gmail.com</translation>
    </message>
    <message>
        <source>Team</source>
        <translation>Каманда распрацоўшчыкаў </translation>
    </message>
    <message>
        <source>Developer, Project Admin</source>
        <translation>Распрацоўшчык, кіраўнік праекта</translation>
    </message>
    <message>
        <source>Web Designer</source>
        <translation>Вэб-дызайнер</translation>
    </message>
    <message>
        <source>geugen@users.berlios.de</source>
        <translation type="obsolete">geugen@users.berlios.de</translation>
    </message>
    <message>
        <source>Thanks To</source>
        <translation>Падзякі</translation>
    </message>
    <message>
        <source>Patches for better MacOS X support</source>
        <translation>Выпраўленьні для паляпшэньня падтрымкі MacOS X </translation>
    </message>
    <message>
        <source>www.outofhanwell.com</source>
        <translation type="obsolete">www.outofhanwell.com</translation>
    </message>
    <message>
        <source>Main Application Icon</source>
        <translation>Значок праграмы</translation>
    </message>
    <message>
        <source>Various fixes and improvements</source>
        <translation>Шматлікія выпраўленьні й паляпшэньні</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Памылка</translation>
    </message>
    <message>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation>Файл &apos;%1&apos; ня знойдзены</translation>
    </message>
    <message>
        <source>Make sure that the program is installed correctly.</source>
        <translation>Пераканайцеся што праграма ўсталявана карэктна</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>Добра</translation>
    </message>
    <message>
        <source>Could not open file &apos;%1&apos;</source>
        <translation type="obsolete">Невозможно открыть файл &apos;%1&apos;</translation>
    </message>
    <message>
        <source>http://keepassx.sf.net</source>
        <translation type="obsolete">http://keepassx.sf.net</translation>
    </message>
    <message>
        <source>Developer</source>
        <translation>Распрацоўшчык</translation>
    </message>
    <message>
        <source>Information on how to translate KeePassX can be found under:</source>
        <translation>Інфармацыя аб перакладзе KeePassX на іншыя мовы знаходзіцца па адрасе:</translation>
    </message>
    <message>
        <source>Current Translation</source>
        <translation>Бягучая мова</translation>
    </message>
    <message>
        <source>None</source>
        <comment>Please replace &apos;None&apos; with the language of your translation</comment>
        <translation>None</translation>
    </message>
    <message>
        <source>Author</source>
        <translation>Аўтар</translation>
    </message>
</context>
<context>
    <name>AboutDlg</name>
    <message>
        <source>About</source>
        <translation>Аб праграме</translation>
    </message>
    <message>
        <source>License</source>
        <translation>Ліцэнзія</translation>
    </message>
    <message>
        <source>Translation</source>
        <translation>Пераклад</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:Sans Serif; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;KeePassX&lt;/span&gt; - Cross Platform Password Manager&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:Sans Serif; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;KeePassX&lt;/span&gt; - Кроссплатформенный менеджер паролей&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Copyright (C) 2005 - 2006 Tarek Saidi 
KeePassX is distributed under the terms of the 
General Public License (GPL) version 2.</source>
        <translation type="obsolete">Copyright (C) 2005 - 2006 Tarek Saidi 
KeePassX is distributed under the terms of the 
General Public License (GPL) version 2.</translation>
    </message>
    <message>
        <source>Credits</source>
        <translation>Падзякі</translation>
    </message>
    <message>
        <source>http://keepassx.sourceforge.net</source>
        <translation>keepassx@gmail.com</translation>
    </message>
    <message>
        <source>keepassx@gmail.com</source>
        <translation>keepassx@gmail.com</translation>
    </message>
    <message>
        <source>AppName</source>
        <translation>AppName</translation>
    </message>
    <message>
        <source>AppFunc</source>
        <translation>AppFunc</translation>
    </message>
    <message>
        <source>Copyright (C) 2005 - 2009 KeePassX Team
KeePassX is distributed under the terms of the
General Public License (GPL) version 2.</source>
        <translation>Copyright (C) 2005 - 2009 KeePassX Team
KeePassX распаўсюджваецца пад ліцэнзіяй
General Public License (GPL) version 2.</translation>
    </message>
</context>
<context>
    <name>AddBookmarkDlg</name>
    <message>
        <source>Add Bookmark</source>
        <translation>Дадаць закладку</translation>
    </message>
    <message>
        <source>Title:</source>
        <translation>Назва:</translation>
    </message>
    <message>
        <source>File:</source>
        <translation>Файл:</translation>
    </message>
    <message>
        <source>Browse...</source>
        <translation>Агляд...</translation>
    </message>
    <message>
        <source>Edit Bookmark</source>
        <translation>Рэдагаваць закладку</translation>
    </message>
    <message>
        <source>KeePass Databases (*.kdb)</source>
        <translation>KeePass база пароляў (*.kdb)</translation>
    </message>
    <message>
        <source>All Files (*)</source>
        <translation>Усе файлы (*)</translation>
    </message>
</context>
<context>
    <name>AutoType</name>
    <message>
        <source>Error</source>
        <translation type="obsolete">Ошибка</translation>
    </message>
</context>
<context>
    <name>AutoTypeDlg</name>
    <message>
        <source>KeePassX - Auto-Type</source>
        <translation>KeePassX - Аўтаўвод</translation>
    </message>
    <message>
        <source>Click on an entry to auto-type it.</source>
        <translation>Націскненьне на запіс актывуе функцыю аўтаўводу.</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>Група</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>Назва</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Імя карыстальніка</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
    <message>
        <source>Auto-Type</source>
        <translation>Аўтаўвод</translation>
    </message>
</context>
<context>
    <name>CAboutDialog</name>
    <message>
        <source>KeePassX %1</source>
        <translation type="obsolete">KeePassX %1</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Ошибка</translation>
    </message>
    <message>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation type="obsolete">Файл &apos;%1&apos; не найден.</translation>
    </message>
    <message>
        <source>Make sure that the program is installed correctly.</source>
        <translation type="obsolete">Убедитесь что программа установлена корректно.</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Could not open file &apos;%1&apos;</source>
        <translation type="obsolete">Невозможно открыть файл &apos;%1&apos;</translation>
    </message>
    <message>
        <source>&lt;b&gt;Current Translation: None&lt;/b&gt;&lt;br&gt;&lt;br&gt;</source>
        <comment>Please replace &apos;None&apos; with the language of your translation</comment>
        <translation type="obsolete">&lt;b&gt;Current Translation: Russian&lt;/b&gt;&lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;Author:&lt;/b&gt; %1&lt;br&gt;</source>
        <translation type="obsolete">&lt;b&gt;Автор:&lt;/b&gt; %1&lt;br&gt;</translation>
    </message>
    <message>
        <source>$TRANSLATION_AUTHOR_EMAIL</source>
        <comment>Here you can enter your email or homepage if you want.</comment>
        <translation type="obsolete">dmitry.funk@gmail.com</translation>
    </message>
    <message>
        <source>$TRANSLATION_AUTHOR</source>
        <translation type="obsolete">Дмитрий Функ</translation>
    </message>
    <message>
        <source>Team</source>
        <translation type="obsolete">Команда разработчиков</translation>
    </message>
    <message>
        <source>Developer, Project Admin</source>
        <translation type="obsolete">Разработчик, руководитель проекта</translation>
    </message>
    <message>
        <source>Web Designer</source>
        <translation type="obsolete">Web дизайнер</translation>
    </message>
    <message>
        <source>geugen@users.berlios.de</source>
        <translation type="obsolete">geugen@users.berlios.de</translation>
    </message>
    <message>
        <source>Thanks To</source>
        <translation type="obsolete">Благодарность</translation>
    </message>
    <message>
        <source>Patches for better MacOS X support</source>
        <translation type="obsolete">Исправления для улучшения поддержки MacOS X</translation>
    </message>
    <message>
        <source>www.outofhanwell.com</source>
        <translation type="obsolete">www.outofhanwell.com</translation>
    </message>
    <message>
        <source>Information on how to translate KeePassX can be found under:
http://keepassx.sourceforge.net/</source>
        <translation type="obsolete">Информацию по переводу KeePassX можно найти на:
http://keepassx.sourceforge.net/</translation>
    </message>
    <message>
        <source>Main Application Icon</source>
        <translation type="obsolete">Значок программы</translation>
    </message>
    <message>
        <source>http://keepassx.sf.net</source>
        <translation type="obsolete">http://keepassx.sf.net</translation>
    </message>
</context>
<context>
    <name>CDbSettingsDlg</name>
    <message>
        <source>AES(Rijndael):  256 Bit   (default)</source>
        <translation>AES(Rijndael):  256 біт   (па змоўчаньні)</translation>
    </message>
    <message>
        <source>Twofish:  256 Bit</source>
        <translation>Twofish:  256 біт</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation>Увага</translation>
    </message>
    <message>
        <source>Please determine the number of encryption rounds.</source>
        <translation>Калі ласка абвярыце колькасьць цыклаў шыфраваньня.</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>Добра</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Памылка</translation>
    </message>
    <message>
        <source>&apos;%1&apos; is not valid integer value.</source>
        <translation>&apos;%1&apos; не карэктная цэлая велічыня</translation>
    </message>
    <message>
        <source>The number of encryption rounds have to be greater than 0.</source>
        <translation>Колькасьць цыклаў шыфраваньня павінна быць больш за нуль</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Наладкі</translation>
    </message>
</context>
<context>
    <name>CEditEntryDlg</name>
    <message>
        <source>Warning</source>
        <translation>Увага</translation>
    </message>
    <message>
        <source>Password and password repetition are not equal.
Please check your input.</source>
        <translation>Паролі не супадаюць.
Праверце ўведзенныя дадзеныя.</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>Добра</translation>
    </message>
    <message>
        <source>Save Attachment...</source>
        <translation>Захаваць укладаньне...</translation>
    </message>
    <message>
        <source>Overwrite?</source>
        <translation type="obsolete">Перезаписать?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>Так</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Нет</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Памылка</translation>
    </message>
    <message>
        <source>Could not remove old file.</source>
        <translation type="obsolete">Невозможно удалить старый файл.</translation>
    </message>
    <message>
        <source>Could not create new file.</source>
        <translation type="obsolete">Невозможно создать новый файл.</translation>
    </message>
    <message>
        <source>Error while writing the file.</source>
        <translation>Памылка запісу ў файл</translation>
    </message>
    <message>
        <source>Delete Attachment?</source>
        <translation>Выдаліць укладаньне?</translation>
    </message>
    <message>
        <source>You are about to delete the attachment of this entry.
Are you sure?</source>
        <translation>Укладаньне будзе выдаленае.
Працягнуць?</translation>
    </message>
    <message>
        <source>No, Cancel</source>
        <translation>Не, адмена</translation>
    </message>
    <message>
        <source>Edit Entry</source>
        <translation>Зьмяніць запіс</translation>
    </message>
    <message>
        <source>Could not open file.</source>
        <translation>Немагчыма адчыніць файл.</translation>
    </message>
    <message>
        <source>%1 Bit</source>
        <translation>%1 біт</translation>
    </message>
    <message>
        <source>Add Attachment...</source>
        <translation>Дадаць укладаньне...</translation>
    </message>
    <message>
        <source>The chosen entry has no attachment or it is empty.</source>
        <translation>Абраны запіс не зьмяшчае ўкладаньне, ці зьмяшчае ўкладаньне нулявой велічыні.</translation>
    </message>
    <message>
        <source>Today</source>
        <translation>Сёньня</translation>
    </message>
    <message>
        <source>1 Week</source>
        <translation>1 тыдзень</translation>
    </message>
    <message>
        <source>2 Weeks</source>
        <translation>2 тыдні</translation>
    </message>
    <message>
        <source>3 Weeks</source>
        <translation>3 тыдні</translation>
    </message>
    <message>
        <source>1 Month</source>
        <translation>1 месяц</translation>
    </message>
    <message>
        <source>3 Months</source>
        <translation>3 месяцы</translation>
    </message>
    <message>
        <source>6 Months</source>
        <translation>6 месяцаў</translation>
    </message>
    <message>
        <source>1 Year</source>
        <translation>1 год</translation>
    </message>
    <message>
        <source>Calendar...</source>
        <translation>Каляндар...</translation>
    </message>
    <message>
        <source>[Untitled Entry]</source>
        <translation>[Безназоўны запіс]</translation>
    </message>
    <message>
        <source>New Entry</source>
        <translation>Новы запіс</translation>
    </message>
</context>
<context>
    <name>CGenPwDialog</name>
    <message>
        <source>Notice</source>
        <translation type="obsolete">Предупреждение</translation>
    </message>
    <message>
        <source>You need to enter at least one character</source>
        <translation type="obsolete">Необходимо ввести более одного символа</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Ошибка</translation>
    </message>
    <message>
        <source>Could not open &apos;/dev/random&apos; or &apos;/dev/urandom&apos;.</source>
        <translation type="obsolete">Невозможно открыть &apos;/dev/random&apos; или &apos;/dev/urandom&apos;.</translation>
    </message>
    <message>
        <source>Password Generator</source>
        <translation>Генэратар пароляў</translation>
    </message>
    <message>
        <source>%1 Bit</source>
        <translation type="obsolete">%1 бит</translation>
    </message>
    <message>
        <source>%1 Bits</source>
        <translation>%1 бітаў</translation>
    </message>
</context>
<context>
    <name>CPasswordDialog</name>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Ошибка</translation>
    </message>
    <message>
        <source>Please enter a Password.</source>
        <translation type="obsolete">Введите пароль.</translation>
    </message>
    <message>
        <source>Please choose a key file.</source>
        <translation type="obsolete">Выберите файл-ключ.</translation>
    </message>
    <message>
        <source>Please enter a Password or select a key file.</source>
        <translation type="obsolete">Введите пароль или выберите файл-ключ.</translation>
    </message>
    <message>
        <source>Database Key</source>
        <translation type="obsolete">Ключ базы паролей</translation>
    </message>
    <message>
        <source>Select a Key File</source>
        <translation type="obsolete">Выбор файл-ключа</translation>
    </message>
    <message>
        <source>*.key</source>
        <translation type="obsolete">*.key</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="obsolete">Внимание</translation>
    </message>
    <message>
        <source>Password an password repetition are not equal.
Please check your input.</source>
        <translation type="obsolete">Пароль и повтор пароля не эквивалентны.
Проверьте введённые данные.</translation>
    </message>
    <message>
        <source>Please enter a password or select a key file.</source>
        <translation type="obsolete">Введите пароль или выберите файл-ключ.</translation>
    </message>
    <message>
        <source>A file with the name &apos;pwsafe.key&apos; already exisits in the given directory.
Do you want to replace it?</source>
        <translation type="obsolete">Файл с именем &apos;pwsafe.key&apos; уже существует в данной директории.
Заменить его?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Да</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Нет</translation>
    </message>
    <message>
        <source>The exisiting file is not writable.</source>
        <translation type="obsolete">Ошибка записи в существующий файл.</translation>
    </message>
    <message>
        <source>A file with the this name already exisits.
Do you want to replace it?</source>
        <translation type="obsolete">Файл с таким именем уже существует.
Заменить его?</translation>
    </message>
    <message>
        <source>The selected key file or directory is not readable.
Please check your permissions.</source>
        <translation type="obsolete">Не удалось прочитать выбранный файл-ключ или директорию.
Проверьте права доступа.</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
</context>
<context>
    <name>CSearchDlg</name>
    <message>
        <source>Notice</source>
        <translation type="obsolete">Предупреждение</translation>
    </message>
    <message>
        <source>Please enter a search string.</source>
        <translation type="obsolete">Введите строку для поиска.</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="obsolete">Поиск</translation>
    </message>
</context>
<context>
    <name>CSelectIconDlg</name>
    <message>
        <source>Delete</source>
        <translation>Выдаліць</translation>
    </message>
    <message>
        <source>Add Icons...</source>
        <translation>Дадаць выяву...</translation>
    </message>
    <message>
        <source>Images (%1)</source>
        <translation>Выява (%1)</translation>
    </message>
    <message>
        <source>%1: File could not be loaded.
</source>
        <translation type="obsolete">%1: Файл не может быть загружен.
</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Памылка</translation>
    </message>
    <message>
        <source>Replace...</source>
        <translation>Замяніць...</translation>
    </message>
    <message>
        <source>An error occured while loading the icon(s):
%1</source>
        <translation type="obsolete">Ошибка при загрузке значков:
%1</translation>
    </message>
    <message>
        <source>An error occured while loading the icon.</source>
        <translation>Памылка пры загрузцы выявы.</translation>
    </message>
    <message>
        <source>Add Custom Icon</source>
        <translation>Дадаць сваю выяву</translation>
    </message>
    <message>
        <source>Pick</source>
        <translation>Абраць</translation>
    </message>
    <message>
        <source>%1: File could not be loaded.</source>
        <translation>%1: Запампаваць файл не атрымалася.</translation>
    </message>
    <message>
        <source>An error occured while loading the icon(s):</source>
        <translation>Памылка запампоўкі выяваў:</translation>
    </message>
</context>
<context>
    <name>CSettingsDlg</name>
    <message>
        <source>Settings</source>
        <translation>Наладкі</translation>
    </message>
    <message>
        <source>Select a directory...</source>
        <translation>Абярыце тэчку...</translation>
    </message>
    <message>
        <source>Select an executable...</source>
        <translation>Абярыце файл для выкананьня...</translation>
    </message>
    <message>
        <source>System Language</source>
        <translation>Мова сыстэмы</translation>
    </message>
</context>
<context>
    <name>CalendarDialog</name>
    <message>
        <source>Calendar</source>
        <translation>Каляндар</translation>
    </message>
</context>
<context>
    <name>CollectEntropyDlg</name>
    <message>
        <source>Entropy Collection</source>
        <translation>Збор энтрапіі</translation>
    </message>
    <message>
        <source>Random Number Generator</source>
        <translation>Генэрацыя выпадковых лікаў</translation>
    </message>
    <message>
        <source>Collecting entropy...
Please move the mouse and/or press some keys until enought entropy for a reseed of the random number generator is collected.</source>
        <translation>Збор энтрапіі...
Калі ласка, рухайце мышкай ці націскайце клявішы да моманту, пакуль неабходная колькасьць энтрапіі будзе сабрана.</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; color:#006400;&quot;&gt;Random pool successfully reseeded!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;p, li { white-space: pre-wrap; }&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; color:#006400;&quot;&gt;Неабходная колькасьць энтрапіі для генэрацыі выпадковых лікаў сабрана!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>CustomizeDetailViewDialog</name>
    <message>
        <source>Group</source>
        <translation>Група</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>Назва</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Імя карыстальніка</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <source>Url</source>
        <translation>Спасылка</translation>
    </message>
    <message>
        <source>Comment</source>
        <translation>Камэнтар</translation>
    </message>
    <message>
        <source>Attachment Name</source>
        <translation>Імя ўкладаньня</translation>
    </message>
    <message>
        <source>Creation Date</source>
        <translation>Дата стварэньня</translation>
    </message>
    <message>
        <source>Last Access Date</source>
        <translation>Дата апошняга доступу</translation>
    </message>
    <message>
        <source>Last Modification Date</source>
        <translation>Дата апошняе зьмены</translation>
    </message>
    <message>
        <source>Expiration Date</source>
        <translation>Тэрмін вартасьці</translation>
    </message>
    <message>
        <source>Time till Expiration</source>
        <translation>Час да канца тэрміна вартасьці</translation>
    </message>
    <message>
        <source>Dialog</source>
        <translation>Дыялёг</translation>
    </message>
    <message>
        <source>Rich Text Editor</source>
        <translation>Рэдактар тэксту</translation>
    </message>
    <message>
        <source>Bold</source>
        <translation>Тоўсты</translation>
    </message>
    <message>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <source>Italic</source>
        <translation>Курсыў</translation>
    </message>
    <message>
        <source>I</source>
        <translation>I</translation>
    </message>
    <message>
        <source>Underlined</source>
        <translation>Падкрэснуты</translation>
    </message>
    <message>
        <source>U</source>
        <translation>U</translation>
    </message>
    <message>
        <source>Left-Aligned</source>
        <translation>Выраўнаваны налева</translation>
    </message>
    <message>
        <source>L</source>
        <translation>L</translation>
    </message>
    <message>
        <source>Centered</source>
        <translation>Цэнтраваны</translation>
    </message>
    <message>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <source>Right-Aligned</source>
        <translation>Выраўнаваны направа</translation>
    </message>
    <message>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <source>Justified</source>
        <translation>Выраўнаваны</translation>
    </message>
    <message>
        <source>Text Color</source>
        <translation>Колер тэксту</translation>
    </message>
    <message>
        <source>Font Size</source>
        <translation>Памер шрыфта</translation>
    </message>
    <message>
        <source>6</source>
        <translation>6</translation>
    </message>
    <message>
        <source>7</source>
        <translation>7</translation>
    </message>
    <message>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <source>9</source>
        <translation>9</translation>
    </message>
    <message>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <source>11</source>
        <translation>11</translation>
    </message>
    <message>
        <source>12</source>
        <translation>12</translation>
    </message>
    <message>
        <source>14</source>
        <translation>13</translation>
    </message>
    <message>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <source>18</source>
        <translation>18</translation>
    </message>
    <message>
        <source>20</source>
        <translation>20</translation>
    </message>
    <message>
        <source>22</source>
        <translation>22</translation>
    </message>
    <message>
        <source>24</source>
        <translation>24</translation>
    </message>
    <message>
        <source>26</source>
        <translation>26</translation>
    </message>
    <message>
        <source>28</source>
        <translation>28</translation>
    </message>
    <message>
        <source>36</source>
        <translation>36</translation>
    </message>
    <message>
        <source>42</source>
        <translation>42</translation>
    </message>
    <message>
        <source>78</source>
        <translation>78</translation>
    </message>
    <message>
        <source>Templates</source>
        <translation>Шаблён</translation>
    </message>
    <message>
        <source>T</source>
        <translation>T</translation>
    </message>
    <message>
        <source>HTML</source>
        <translation>HTML</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
</context>
<context>
    <name>Database</name>
    <message>
        <source>Never</source>
        <translation>Ніколі</translation>
    </message>
</context>
<context>
    <name>DatabaseSettingsDlg</name>
    <message>
        <source>Database Settings</source>
        <translation>Наладка базы пароляў</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation>Шыфраваньне</translation>
    </message>
    <message>
        <source>Algorithm:</source>
        <translation>Алгарытм:</translation>
    </message>
    <message>
        <source>Encryption Rounds:</source>
        <translation>Цыклаў шыфраваньня:</translation>
    </message>
    <message>
        <source>Calculate rounds for a 1-second delay on this computer</source>
        <translation>Вылічэньне колькасьці цыклаў, якія могуць быць апрацаваны за адну сэкунду на гэтым кампутары</translation>
    </message>
</context>
<context>
    <name>DetailViewTemplate</name>
    <message>
        <source>Group</source>
        <translation>Група</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>Назва</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Імя карыстальніка</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <source>URL</source>
        <translation>Спасылка</translation>
    </message>
    <message>
        <source>Creation</source>
        <translation>Стварэньне</translation>
    </message>
    <message>
        <source>Last Access</source>
        <translation>Апошні доступ</translation>
    </message>
    <message>
        <source>Last Modification</source>
        <translation>Дата апошніх зьменаў</translation>
    </message>
    <message>
        <source>Expiration</source>
        <translation>Тэрмін вартасьці</translation>
    </message>
    <message>
        <source>Comment</source>
        <translation>Камэнтар</translation>
    </message>
</context>
<context>
    <name>EditEntryDialog</name>
    <message>
        <source>Edit Entry</source>
        <translation>Зьмяніць запіс</translation>
    </message>
    <message>
        <source>Username:</source>
        <translation>Імя карыстальніка:</translation>
    </message>
    <message>
        <source>Password Repet.:</source>
        <translation type="obsolete">Повтор пароля:</translation>
    </message>
    <message>
        <source>Title:</source>
        <translation>Назва:</translation>
    </message>
    <message>
        <source>URL:</source>
        <translation>Спасылка:</translation>
    </message>
    <message>
        <source>Password:</source>
        <translation>Пароль:</translation>
    </message>
    <message>
        <source>Quality:</source>
        <translation>Якасьць:</translation>
    </message>
    <message>
        <source>Comment:</source>
        <translation>Камэнтар:</translation>
    </message>
    <message>
        <source>Expires:</source>
        <translation>Тэрмін вартасьці:</translation>
    </message>
    <message>
        <source>Group:</source>
        <translation>Група:</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Отмена</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
    <message>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <source>Icon:</source>
        <translation>Значок:</translation>
    </message>
    <message>
        <source>Ge&amp;n.</source>
        <translation>&amp;Генэрацыя.</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="obsolete">...</translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation type="obsolete">O&amp;K</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation type="obsolete">Alt+K</translation>
    </message>
    <message>
        <source>Never</source>
        <translation>Ніколі</translation>
    </message>
    <message>
        <source>Attachment:</source>
        <translation>Укладаньне:</translation>
    </message>
    <message>
        <source>&gt;</source>
        <translation type="obsolete">&gt;</translation>
    </message>
    <message>
        <source>%1 Bit</source>
        <translation>%1 біт</translation>
    </message>
    <message>
        <source>Repeat:</source>
        <translation>Паўтор пароля:</translation>
    </message>
</context>
<context>
    <name>EditGroupDialog</name>
    <message>
        <source>Group Properties</source>
        <translation>Парамэтры групы</translation>
    </message>
    <message>
        <source>Title:</source>
        <translation>Назва:</translation>
    </message>
    <message>
        <source>Icon:</source>
        <translation>Значок:</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Отмена</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation type="obsolete">O&amp;K</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation type="obsolete">Alt+K</translation>
    </message>
    <message>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
</context>
<context>
    <name>ExpiredEntriesDialog</name>
    <message>
        <source>Expired Entries</source>
        <translation>Запісы з пратэрмінаваным тэрмінам</translation>
    </message>
    <message>
        <source>Double click on an entry to jump to it.</source>
        <translation>Падвоены націск на гэты запіс.</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>Група</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>Назва</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Імя карыстальніка</translation>
    </message>
    <message>
        <source>Expired</source>
        <translation>Тэрмін прыдатнасьці скончыўся</translation>
    </message>
    <message>
        <source>Expired Entries in the Database</source>
        <translation>Запісы ў базе дадзеных з скончаным тэрмінам прыдатнасьці</translation>
    </message>
</context>
<context>
    <name>Export_KeePassX_Xml</name>
    <message>
        <source>XML Files (*.xml)</source>
        <translation>XML-файлы (*.xml)</translation>
    </message>
    <message>
        <source>All Files (*)</source>
        <translation>Усе файлы (*)</translation>
    </message>
    <message>
        <source>KeePassX XML File</source>
        <translation>KeePassX XML-файл</translation>
    </message>
</context>
<context>
    <name>Export_Txt</name>
    <message>
        <source>Could not open file (FileError=%1)</source>
        <translation type="obsolete">Невозможно открыть файл  (FileError=%1)</translation>
    </message>
    <message>
        <source>All Files (*)</source>
        <translation>Усе файлы (*)</translation>
    </message>
    <message>
        <source>Text Files (*.txt)</source>
        <translation>Тэкставыя файлы (*.txt)</translation>
    </message>
    <message>
        <source>Text File</source>
        <translation>Тэкставы файл</translation>
    </message>
</context>
<context>
    <name>ExporterBase</name>
    <message>
        <source>Export Failed</source>
        <translation>Няўдалы экспарт</translation>
    </message>
    <message>
        <source>Export File...</source>
        <translation>Файл для экспарту...</translation>
    </message>
</context>
<context>
    <name>FileErrors</name>
    <message>
        <source>No error occurred.</source>
        <translation>Памылак не ўзьнікла.</translation>
    </message>
    <message>
        <source>An error occurred while reading from the file.</source>
        <translation>Памылка чытаньня з файлу.</translation>
    </message>
    <message>
        <source>An error occurred while writing to the file.</source>
        <translation>Памылка запісу ў файл.</translation>
    </message>
    <message>
        <source>A fatal error occurred.</source>
        <translation>Адбылася фатальная памылка.</translation>
    </message>
    <message>
        <source>An resource error occurred.</source>
        <translation>Адбылася памылка рэсурсаў.</translation>
    </message>
    <message>
        <source>The file could not be opened.</source>
        <translation>Немагчыма адчыніць файл.</translation>
    </message>
    <message>
        <source>The operation was aborted.</source>
        <translation>Апэрацыя была спыненая.</translation>
    </message>
    <message>
        <source>A timeout occurred.</source>
        <translation>Час чаканьня скончыўся.</translation>
    </message>
    <message>
        <source>An unspecified error occurred.</source>
        <translation>Адбылася невядомая памылка.</translation>
    </message>
    <message>
        <source>The file could not be removed.</source>
        <translation>Немагчыма выдаліць файл.</translation>
    </message>
    <message>
        <source>The file could not be renamed.</source>
        <translation>Немагчыма перайменаваць файл.</translation>
    </message>
    <message>
        <source>The position in the file could not be changed.</source>
        <translation>Пазыцыя ў файле ня можа быць зьмененая.</translation>
    </message>
    <message>
        <source>The file could not be resized.</source>
        <translation>Немагчыма зьмяніць памер файла.</translation>
    </message>
    <message>
        <source>The file could not be accessed.</source>
        <translation>Няма доступу да файла.</translation>
    </message>
    <message>
        <source>The file could not be copied.</source>
        <translation>Немагчыма скапіяваць файл.</translation>
    </message>
</context>
<context>
    <name>GenPwDlg</name>
    <message>
        <source>Alt+U</source>
        <translation type="obsolete">Alt+U</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <source>Alt+M</source>
        <translation type="obsolete">Alt+M</translation>
    </message>
    <message>
        <source>Alt+L</source>
        <translation type="obsolete">Alt+L</translation>
    </message>
    <message>
        <source>Password Generator</source>
        <translation>Генэратар пароляў</translation>
    </message>
    <message>
        <source>Accep&amp;t</source>
        <translation type="obsolete">&amp;Принять</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Отмена</translation>
    </message>
    <message>
        <source>Generate</source>
        <translation>Генэраваць</translation>
    </message>
    <message>
        <source>New Password:</source>
        <translation>Новы пароль:</translation>
    </message>
    <message>
        <source>Quality:</source>
        <translation>Якасьць:</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Опцыі</translation>
    </message>
    <message>
        <source>&amp;Upper Letters</source>
        <translation>&amp;Вялікія літары</translation>
    </message>
    <message>
        <source>&amp;Lower Letters</source>
        <translation>&amp;Малыя літары</translation>
    </message>
    <message>
        <source>&amp;Numbers</source>
        <translation>&amp;Лічбы</translation>
    </message>
    <message>
        <source>&amp;Special Characters</source>
        <translation>&amp;Спэцыяльныя сымбалі</translation>
    </message>
    <message>
        <source>Minus</source>
        <translation type="obsolete">Знак минуса</translation>
    </message>
    <message>
        <source>U&amp;nderline</source>
        <translation type="obsolete">По&amp;дчёркивание</translation>
    </message>
    <message>
        <source>h&amp;igher ANSI-Characters</source>
        <translation type="obsolete">ANSI-символы &amp;второй половины таблицы</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation type="obsolete">Alt+H</translation>
    </message>
    <message>
        <source>Use &amp;only following characters:</source>
        <translation type="obsolete">&amp;Только следующие символы:</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation type="obsolete">Alt+O</translation>
    </message>
    <message>
        <source>Length:</source>
        <translation>Даўжыня:</translation>
    </message>
    <message>
        <source>Use &quot;/dev/rando&amp;m&quot;</source>
        <translation type="obsolete">Использовать  &quot;/dev/rando&amp;m&quot;</translation>
    </message>
    <message>
        <source>Use follo&amp;wing character groups:</source>
        <translation type="obsolete">Только следующие &amp;группы символов:</translation>
    </message>
    <message>
        <source>Alt+W</source>
        <translation type="obsolete">Alt+W</translation>
    </message>
    <message>
        <source>White &amp;Spaces</source>
        <translation type="obsolete">&amp;Пробелы</translation>
    </message>
    <message>
        <source>Alt+S</source>
        <translation type="obsolete">Alt+S</translation>
    </message>
    <message>
        <source>Enable entropy collection</source>
        <translation>Уключыць механізм збору энтрапіі</translation>
    </message>
    <message>
        <source>Collect only once per session</source>
        <translation>Збор толькі адзін раз за сэсію</translation>
    </message>
    <message>
        <source>Random</source>
        <translation>Выпадковыя лікі</translation>
    </message>
    <message>
        <source>&amp;Underline</source>
        <translation>Па&amp;дкрэсьленьне</translation>
    </message>
    <message>
        <source>&amp;White Spaces</source>
        <translation>&amp;Прабелы</translation>
    </message>
    <message>
        <source>&amp;Minus</source>
        <translation>&amp;Знак мінуса</translation>
    </message>
    <message>
        <source>Exclude look-alike characters</source>
        <translation>Выключыць падобныя сымбалі</translation>
    </message>
    <message>
        <source>Ensure that password contains characters from every group</source>
        <translation>Паролі павінны ўключаць ў сябе сымбалі з ўсіх групаў</translation>
    </message>
    <message>
        <source>Pronounceable</source>
        <translation>Паролі лёгкія на вымаўленьне</translation>
    </message>
    <message>
        <source>Lower Letters</source>
        <translation>Маленькія літары</translation>
    </message>
    <message>
        <source>Upper Letters</source>
        <translation>Вялікія літары</translation>
    </message>
    <message>
        <source>Numbers</source>
        <translation>Лічбы</translation>
    </message>
    <message>
        <source>Special Characters</source>
        <translation>Спэцыяльныя сымбалі</translation>
    </message>
    <message>
        <source>Use following character groups:</source>
        <translation>Выкарыстоўваць наступныя групы сымбаляў:</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation>Свой варыянт</translation>
    </message>
    <message>
        <source>Use the following characters:</source>
        <translation>Выкарыстоўваць наступныя сымбалі::</translation>
    </message>
</context>
<context>
    <name>HelpDlg</name>
    <message>
        <source>Previous Page</source>
        <translation>Папярэдняя старонка</translation>
    </message>
    <message>
        <source>Next Page</source>
        <translation>Наступная старонка</translation>
    </message>
    <message>
        <source>First Page</source>
        <translation>У пачатак</translation>
    </message>
    <message>
        <source>Help Contents</source>
        <translation>Зьмест</translation>
    </message>
</context>
<context>
    <name>Import_KWalletXml</name>
    <message>
        <source>XML Files (*.xml)</source>
        <translation>XML-файлы (*.xml)</translation>
    </message>
    <message>
        <source>All Files (*)</source>
        <translation>Усе файлы (*)</translation>
    </message>
    <message>
        <source>Import Failed</source>
        <translation>Імпарт завяршыўся няўдала</translation>
    </message>
    <message>
        <source>Invalid XML data (see stdout for details).</source>
        <translation>Памылка ў фармаце дадзеных XML (см. stdout).</translation>
    </message>
    <message>
        <source>Invalid XML file.</source>
        <translation>Няслушны файл XML.</translation>
    </message>
    <message>
        <source>Document does not contain data.</source>
        <translation>Дакумэнт бяз дадзеных</translation>
    </message>
</context>
<context>
    <name>Import_KeePassX_Xml</name>
    <message>
        <source>KeePass XML Files (*.xml)</source>
        <translation>KeePassX XML-файл (*.xml)</translation>
    </message>
    <message>
        <source>All Files (*)</source>
        <translation>Усе файлы (*)</translation>
    </message>
    <message>
        <source>Import Failed</source>
        <translation>Імпарт завяршыўся няўдала</translation>
    </message>
    <message>
        <source>XML parsing error on line %1 column %2:
%3</source>
        <translation>Памылка ў фармаце дадзеных XML. Радок %1, слупок %2:%3</translation>
    </message>
    <message>
        <source>Parsing error: File is no valid KeePassX XML file.</source>
        <translation>Памылка ў фармаце дадзеных: няправільны KeePassX XML файл.</translation>
    </message>
</context>
<context>
    <name>Import_PwManager</name>
    <message>
        <source>PwManager Files (*.pwm)</source>
        <translation>Файл PwManager (*.pwm)</translation>
    </message>
    <message>
        <source>All Files (*)</source>
        <translation>Усе файлы (*)</translation>
    </message>
    <message>
        <source>Import Failed</source>
        <translation>Імпарт скончыўся няўдала</translation>
    </message>
    <message>
        <source>File is empty.</source>
        <translation>Файл пусты.</translation>
    </message>
    <message>
        <source>File is no valid PwManager file.</source>
        <translation>Няправільны PwManager файл.</translation>
    </message>
    <message>
        <source>Unsupported file version.</source>
        <translation>Вэрсія файла не падтрымліваецца</translation>
    </message>
    <message>
        <source>Unsupported hash algorithm.</source>
        <translation>Хэш алгарытм не падтрымліваецца</translation>
    </message>
    <message>
        <source>Unsupported encryption algorithm.</source>
        <translation>Невядомы алгарытм шыфраваньня.</translation>
    </message>
    <message>
        <source>Compressed files are not supported yet.</source>
        <translation>Сьціснутыя файлы, на жаль, пакуль не падтрымліваюцца.</translation>
    </message>
    <message>
        <source>Wrong password.</source>
        <translation>Пароль няслушны.</translation>
    </message>
    <message>
        <source>File is damaged (hash test failed).</source>
        <translation>Файл пашкоджаны (няслушны хэш-код).</translation>
    </message>
    <message>
        <source>Invalid XML data (see stdout for details).</source>
        <translation>Памылка ў фармаце дадзеных XML (гл. stdout).</translation>
    </message>
</context>
<context>
    <name>ImporterBase</name>
    <message>
        <source>Import File...</source>
        <translation>Файл для імпарту...</translation>
    </message>
    <message>
        <source>Import Failed</source>
        <translation>Імпарт завяршыўся няўдала</translation>
    </message>
</context>
<context>
    <name>Kdb3Database</name>
    <message>
        <source>Could not open file.</source>
        <translation>Немагчыма адчыніць файл.</translation>
    </message>
    <message>
        <source>Unexpected file size (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</source>
        <translation>Нечаканы памер файла (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</translation>
    </message>
    <message>
        <source>Wrong Signature</source>
        <translation>Няслушная сігнатура</translation>
    </message>
    <message>
        <source>Unsupported File Version.</source>
        <translation>Гэтая вэрсія файла не падтрымліваецца.</translation>
    </message>
    <message>
        <source>Unknown Encryption Algorithm.</source>
        <translation>Невядомы алгарытм шыфраваньня.</translation>
    </message>
    <message>
        <source>Unable to initialize the twofish algorithm.</source>
        <translation>Памылкі ініцыялізацыі алгарытма twofish.</translation>
    </message>
    <message>
        <source>Unknown encryption algorithm.</source>
        <translation>Невядомы алгарытм шыфраваньня.</translation>
    </message>
    <message>
        <source>Decryption failed.
The key is wrong or the file is damaged.</source>
        <translation>Расшыфроўка спыніна.
Няслушны ключ ці файл пашкоджаны.</translation>
    </message>
    <message>
        <source>Hash test failed.
The key is wrong or the file is damaged.</source>
        <translation>Няслушны хэш-код. Ключ няслушны ці файл пашкоджаны.</translation>
    </message>
    <message>
        <source>Invalid group tree.</source>
        <translation>Памылка ў дрэве групаў.</translation>
    </message>
    <message>
        <source>Key file is empty.</source>
        <translation>Файл пусты.</translation>
    </message>
    <message>
        <source>The database must contain at least one group.</source>
        <translation>База пароляў павінна мець як мінімум адну групу.</translation>
    </message>
    <message>
        <source>Could not open file for writing.</source>
        <translation type="obsolete">Невозможно открыть файл для записи.</translation>
    </message>
    <message>
        <source>Unexpected error: Offset is out of range.</source>
        <translation>Нечаканая памылка: індэкс выйшаў за межы дапушчальнага.</translation>
    </message>
    <message>
        <source>Unable to initalize the twofish algorithm.</source>
        <translation type="obsolete">Ошибка инициализации алгоритма twofish.</translation>
    </message>
    <message>
        <source>The database has been opened read-only.</source>
        <translation>База пароляў адчынена ў рэжыме &quot;толькі чытаньне&quot;.</translation>
    </message>
</context>
<context>
    <name>Kdb3Database::EntryHandle</name>
    <message>
        <source>Bytes</source>
        <translation>Байт</translation>
    </message>
    <message>
        <source>KiB</source>
        <translation>KiB</translation>
    </message>
    <message>
        <source>MiB</source>
        <translation>MiB</translation>
    </message>
    <message>
        <source>GiB</source>
        <translation>GiB</translation>
    </message>
</context>
<context>
    <name>KeepassEntryView</name>
    <message>
        <source>Title</source>
        <translation>Назва</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Імя карыстальніка</translation>
    </message>
    <message>
        <source>URL</source>
        <translation>Спасылка</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <source>Comments</source>
        <translation>Камэнтар</translation>
    </message>
    <message>
        <source>Expires</source>
        <translation>Сканчэньне</translation>
    </message>
    <message>
        <source>Creation</source>
        <translation>Стварэньне</translation>
    </message>
    <message>
        <source>Last Change</source>
        <translation>Апошняя зьмена</translation>
    </message>
    <message>
        <source>Last Access</source>
        <translation>Апошні доступ</translation>
    </message>
    <message>
        <source>Attachment</source>
        <translation>Прычэпак</translation>
    </message>
    <message>
        <source>%1 items</source>
        <translation type="obsolete">%1 пунктов</translation>
    </message>
    <message>
        <source>Delete?</source>
        <translation>Выдаліць?</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>Група</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Памылка</translation>
    </message>
    <message>
        <source>At least one group must exist before adding an entry.</source>
        <translation>Каб можна было дадаць запіс павінна існаваць як мінімум адна група.</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>Добра</translation>
    </message>
    <message>
        <source>Are you sure you want to delete this entry?</source>
        <translation>Запіс будзе выдалены. Працягнуць?</translation>
    </message>
    <message>
        <source>Are you sure you want to delete these %1 entries?</source>
        <translation>%1 запісаў будзе выдалена. Працягнуць?</translation>
    </message>
</context>
<context>
    <name>KeepassGroupView</name>
    <message>
        <source>Search Results</source>
        <translation>Вынікі пошуку</translation>
    </message>
    <message>
        <source>Groups</source>
        <translation type="obsolete">Группы</translation>
    </message>
    <message>
        <source>Delete?</source>
        <translation>Выдаліць?</translation>
    </message>
    <message>
        <source>Are you sure you want to delete this group, all its child groups and all their entries?</source>
        <translation>Група з усімі падгрупамі і ўсімі запісамі будзе выдалена. Працягнуць?</translation>
    </message>
</context>
<context>
    <name>KeepassMainWindow</name>
    <message>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <source>Ctrl+G</source>
        <translation>Ctrl+G</translation>
    </message>
    <message>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <source>Ctrl+B</source>
        <translation>Ctrl+B</translation>
    </message>
    <message>
        <source>Ctrl+U</source>
        <translation>Ctrl+U</translation>
    </message>
    <message>
        <source>Ctrl+Y</source>
        <translation>Ctrl+Y</translation>
    </message>
    <message>
        <source>Ctrl+E</source>
        <translation>Ctrl+E</translation>
    </message>
    <message>
        <source>Ctrl+D</source>
        <translation>Ctrl+D</translation>
    </message>
    <message>
        <source>Ctrl+K</source>
        <translation>Ctrl+K</translation>
    </message>
    <message>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <source>Shift+Ctrl+S</source>
        <translation>Shift+Ctrl+S</translation>
    </message>
    <message>
        <source>Shift+Ctrl+F</source>
        <translation>Shift+Ctrl+F</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Памылка</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Save modified file?</source>
        <translation>Захаваць зьмены у файле?</translation>
    </message>
    <message>
        <source>The current file was modified. Do you want
to save the changes?</source>
        <translation type="obsolete">Текущий файл был изменён. Хотите
сохранить изменения?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Да</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Нет</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
    <message>
        <source>KeePassX - %1</source>
        <translation type="obsolete">KeePassX - %1</translation>
    </message>
    <message>
        <source>&lt;B&gt;Group: &lt;/B&gt;%1  &lt;B&gt;Title: &lt;/B&gt;%2  &lt;B&gt;Username: &lt;/B&gt;%3  &lt;B&gt;URL: &lt;/B&gt;&lt;a href=%4&gt;%4&lt;/a&gt;  &lt;B&gt;Password: &lt;/B&gt;%5  &lt;B&gt;Creation: &lt;/B&gt;%6  &lt;B&gt;Last Change: &lt;/B&gt;%7  &lt;B&gt;LastAccess: &lt;/B&gt;%8  &lt;B&gt;Expires: &lt;/B&gt;%9</source>
        <translation type="obsolete">&lt;B&gt;Группа: &lt;/B&gt;%1  &lt;B&gt;Название: &lt;/B&gt;%2  &lt;B&gt;Имя: &lt;/B&gt;%3  &lt;B&gt;Ссылка: &lt;/B&gt;&lt;a href=%4&gt;%4&lt;/a&gt;  &lt;B&gt;Пароль: &lt;/B&gt;%5  &lt;B&gt;Создано: &lt;/B&gt;%6  &lt;B&gt;Изменено: &lt;/B&gt;%7  &lt;B&gt;Доступ: &lt;/B&gt;%8  &lt;B&gt;Окончание: &lt;/B&gt;%9</translation>
    </message>
    <message>
        <source>Clone Entry</source>
        <translation>Дубляваць запіс</translation>
    </message>
    <message>
        <source>Delete Entry</source>
        <translation>Выдаліць запіс</translation>
    </message>
    <message>
        <source>Clone Entries</source>
        <translation>Дубляваць запісы</translation>
    </message>
    <message>
        <source>Delete Entries</source>
        <translation>Выдаліць запісы</translation>
    </message>
    <message>
        <source>File could not be saved.
%1</source>
        <translation type="obsolete">Невозможно сохранить файл.
%1</translation>
    </message>
    <message>
        <source>Save Database As...</source>
        <translation type="obsolete">Сохранить базу паролей как...</translation>
    </message>
    <message>
        <source>Ready</source>
        <translation>Падрыхтаваны</translation>
    </message>
    <message>
        <source>[new]</source>
        <translation type="obsolete">[новый]</translation>
    </message>
    <message>
        <source>Open Database...</source>
        <translation>Адчыніць базу пароляў...</translation>
    </message>
    <message>
        <source>Loading Database...</source>
        <translation>Загрузка базы пароляў...</translation>
    </message>
    <message>
        <source>Loading Failed</source>
        <translation>Памылка загрузкі</translation>
    </message>
    <message>
        <source>Export To...</source>
        <translation type="obsolete">Экспорт в...</translation>
    </message>
    <message>
        <source>Unknown error in Import_PwManager::importFile()()</source>
        <translation type="obsolete">Неизвестная ошибка в Import_PwManager::importFile()()</translation>
    </message>
    <message>
        <source>Unknown error in Import_KWalletXml::importFile()</source>
        <translation type="obsolete">Неизвестная ошибка в Import_KWalletXml::importFile()</translation>
    </message>
    <message>
        <source>Unknown error in PwDatabase::openDatabase()</source>
        <translation type="obsolete">Неизвестная ошибка в PwDatabase::openDatabase()</translation>
    </message>
    <message>
        <source>Ctrl+V</source>
        <translation>Ctrl+V</translation>
    </message>
    <message>
        <source>Show Toolbar</source>
        <translation type="obsolete">Отобразить панель инструментов</translation>
    </message>
    <message>
        <source>KeePassX</source>
        <translation type="obsolete">KeePassX</translation>
    </message>
    <message>
        <source>Unknown error while loading database.</source>
        <translation>Невядомая памылка пры загрузцы базы дадзеных.</translation>
    </message>
    <message>
        <source>KeePass Databases (*.kdb)</source>
        <translation>KeePass база пароляў (*.kdb)</translation>
    </message>
    <message>
        <source>All Files (*)</source>
        <translation>Усе файлы (*)</translation>
    </message>
    <message>
        <source>Save Database...</source>
        <translation>Захаваць базу пароляў...</translation>
    </message>
    <message>
        <source>1 Month</source>
        <translation>1 месяц</translation>
    </message>
    <message>
        <source>%1 Months</source>
        <translation>%1 месяца</translation>
    </message>
    <message>
        <source>1 Year</source>
        <translation>1 год</translation>
    </message>
    <message>
        <source>%1 Years</source>
        <translation>%1 гадоў</translation>
    </message>
    <message>
        <source>1 Day</source>
        <translation>1 дзень</translation>
    </message>
    <message>
        <source>%1 Days</source>
        <translation>%1 дзён</translation>
    </message>
    <message>
        <source>less than 1 day</source>
        <translation>менш, чым адзін дзень</translation>
    </message>
    <message>
        <source>Locked</source>
        <translation type="obsolete">заблокировано</translation>
    </message>
    <message>
        <source>Unlocked</source>
        <translation type="obsolete">разблокированно</translation>
    </message>
    <message>
        <source>Ctrl+L</source>
        <translation>Ctrl+L</translation>
    </message>
    <message>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <source>The database file does not exist.</source>
        <translation>Файл базы пароляў ня знойдзены.</translation>
    </message>
    <message>
        <source>new</source>
        <translation>новы</translation>
    </message>
    <message>
        <source>Expired</source>
        <translation>Пратэрмінаваны</translation>
    </message>
    <message>
        <source>Un&amp;lock Workspace</source>
        <translation>&amp;Разблякаваць галоўнае акно</translation>
    </message>
    <message>
        <source>&amp;Lock Workspace</source>
        <translation>&amp;Заблякаваць галоўнае акно</translation>
    </message>
    <message>
        <source>The following error occured while opening the database:</source>
        <translation>Памылка пры адкрыцьці базы пароляў:</translation>
    </message>
    <message>
        <source>File could not be saved.</source>
        <translation>Не атрымалася захаваць файл.</translation>
    </message>
    <message>
        <source>Show &amp;Toolbar</source>
        <translation>&amp;Паказаць панэль інструмэнтаў</translation>
    </message>
    <message>
        <source>Ctrl+P</source>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <source>Ctrl+X</source>
        <translation>Ctrl+X</translation>
    </message>
    <message>
        <source>Ctrl+I</source>
        <translation>Ctrl+I</translation>
    </message>
    <message>
        <source>Database locked</source>
        <translation>База пароляў заблякаваная</translation>
    </message>
    <message>
        <source>The database you are trying to open is locked.
This means that either someone else has opened the file or KeePassX crashed last time it opened the database.

Do you want to open it anyway?</source>
        <translation>База пароляў, якую вы спрабуеце адчыніць, заблякаваная. 
Звычайна такое бывае калі нехта ў дадзены момант ужо адчыніў базу, ці калі KeePassX ня быў нармальна завершаны падчас адчыненай базы.

Працягнуць адкрыцьцё?</translation>
    </message>
    <message>
        <source>The current file was modified.
Do you want to save the changes?</source>
        <translation>База пароляў была зьмененая.
Захаваць зьмены?</translation>
    </message>
    <message>
        <source>Couldn&apos;t remove database lock file.</source>
        <translation>Не атрымалася выдаліць файл блякаваньня базы пароляў.</translation>
    </message>
    <message>
        <source>Open read-only</source>
        <translation>Адчыніць у рэжыме толькі для чытаньня</translation>
    </message>
    <message>
        <source>Couldn&apos;t create lock file. Opening the database read-only.</source>
        <translation>Не атрымалася стварыць файл блякаваньня базы пароляў. База пароляў будзе адчынена толькі для чытаньня.</translation>
    </message>
    <message>
        <source>Information</source>
        <translation type="obsolete">Информация</translation>
    </message>
    <message>
        <source>WARNING! Synchronization has not yet been well tested.
WARNING! It can cause database corruption.

So please BACKUP your databases first!

The synchronization proceeds as following:
1 You choose a database to synchronize with the current database
2 The current and the chosen database will be recursively processed
  - all missing groups and items in one database will be copied
    to the other and vice versa
  - all existing groups matched by title will be synchronized
    (current database has a priority)
  - all existing items matched by title will be synchronized
    (according to last modification time)
  - all groups and items whose title ends with &quot;</source>
        <translation type="obsolete">ВНИМАНИЕ!  Синхронизация еще не протестирована в достаточной степени.
ВНИМАНИЕ!  Синхронизация может повредить одну или обе базы паролей.

Пожалуйста, сделайте РЕЗЕРВНЫЕ КОПИИ баз паролей.

Принцип работы синхронизации:
1 Вы выбираете другую базу паролей для синхронизации с окрытой базой
2 Обе базы паролей обрабатываются рекурсивно
  - все существующие только в одной базе группы и записи переписываются
    в другую
  - все существующие в обеих базах группы и записи синхронизируются
  - все группы и записи имя которых заканчивается на &quot;delete.it&quot; стираются
    из обеех баз паролей</translation>
    </message>
    <message>
        <source>Please, open a database first.</source>
        <translation type="obsolete">Для начала откройте пожалуйста базу паролей.</translation>
    </message>
    <message>
        <source>Open Database to synchronize with...</source>
        <translation type="obsolete">Выбор базы паролей для синхронизации...</translation>
    </message>
    <message>
        <source>Can&apos;t synchronize with the own file.</source>
        <translation type="obsolete">База паролей уже открыта. Выберете другую базу паролей нельзя синхронизации.</translation>
    </message>
    <message>
        <source>Error saving current file</source>
        <translation type="obsolete">Ошибка сохранения файла</translation>
    </message>
    <message>
        <source>The current file was modified and must be saved before synchronization can proceed.
Do you want to save the changes?</source>
        <translation type="obsolete">Базу паролей необходимо сохранить перед началом синхронизации.
Продолжить?</translation>
    </message>
    <message>
        <source>Synchronization complete</source>
        <translation type="obsolete">Синхронизация завершена</translation>
    </message>
    <message>
        <source>Synchronization successfully finished.

</source>
        <translation type="obsolete">Синхронизация завершена.

</translation>
    </message>
    <message>
        <source>Groups processed: </source>
        <translation type="obsolete">Групп обработано: </translation>
    </message>
    <message>
        <source>Groups created: </source>
        <translation type="obsolete">Групп создано: </translation>
    </message>
    <message>
        <source>Groups synchronized: </source>
        <translation type="obsolete">Групп синхронизировано: </translation>
    </message>
    <message>
        <source>Groups deleted: </source>
        <translation type="obsolete">Групп стёрто: </translation>
    </message>
    <message>
        <source>Entries processed: </source>
        <translation type="obsolete">Записей обработано: </translation>
    </message>
    <message>
        <source>Entries created: </source>
        <translation type="obsolete">Записей создано: </translation>
    </message>
    <message>
        <source>Entries synchronized: </source>
        <translation type="obsolete">Записей синхронизировано: </translation>
    </message>
    <message>
        <source>Entries deleted: </source>
        <translation type="obsolete">Записей стёрто: </translation>
    </message>
    <message>
        <source>Unknown error while saving database: </source>
        <translation type="obsolete">Неизвестная ошибка при сохранении базы паролей: </translation>
    </message>
    <message>
        <source>The following error occurred while saving the database:</source>
        <translation type="obsolete">Ошибка при сохранении базы паролей:</translation>
    </message>
    <message>
        <source>Error:</source>
        <translation type="obsolete">Ошибка:</translation>
    </message>
    <message>
        <source>Close failed</source>
        <translation type="obsolete">Ошибка закрытия файла</translation>
    </message>
    <message>
        <source>WARNING! Synchronization has not yet been well tested.
WARNING! It can cause database corruption.

So please BACKUP your databases first!

The synchronization proceeds as following:
1 You choose a database to synchronize with the current database
2 The current and the chosen database will be recursively processed
  - all missing groups and items in one database will be copied
    to the other and vice versa
  - all existing groups matched by title will be synchronized
    (current database has a priority)
  - all existing items matched by title will be synchronized
    (according to last modification time)
  - all groups and items whose title ends with &quot;.deleteit&quot; will be deleted
    in both databases

Please consider:
- No conflicts resolving is possible
- No stored icons synchronization is implemented yet</source>
        <translation type="obsolete">ВНИМАНИЕ!  Синхронизация еще не протестирована в достаточной степени.
ВНИМАНИЕ!  Синхронизация может повредить одну или обе базы паролей.

Пожалуйста, сделайте РЕЗЕРВНЫЕ КОПИИ баз паролей.

Принцип работы синхронизации:
1 Вы выбираете другую базу паролей для синхронизации с окрытой базой
2 Обе базы паролей обрабатываются рекурсивно
  - все существующие только в одной базе группы и записи
    переписываются в другую
  - все существующие в обеих базах группы и записи синхронизируются
  - все группы и записи имя которых заканчивается на &quot;delete.it&quot; стираются
    из обеех баз паролей

Обращаем Ваше внимание на то что:
- Распознование конфликтов не поддерживается
- Синхронизация сохраненных в базе иконок пока не реализована</translation>
    </message>
    <message>
        <source>Couldn&apos;t create database lock file.</source>
        <translation>Не атрымалася стварыць файл блякаваньня базы пароляў.</translation>
    </message>
    <message>
        <source>locked</source>
        <translation>заблякавана</translation>
    </message>
    <message>
        <source>Ctrl+M</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <source>Error</source>
        <translation>Памылка</translation>
    </message>
    <message>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation>Файл &apos;%1&apos; ня знойдзены.</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>KeePassX</source>
        <translation>KeePassX</translation>
    </message>
    <message>
        <source>Columns</source>
        <translation type="obsolete">Столбцы</translation>
    </message>
    <message>
        <source>PwManager File (*.pwm)</source>
        <translation type="obsolete">Файл PwManager (*.pwm)</translation>
    </message>
    <message>
        <source>KWallet XML-File (*.xml)</source>
        <translation type="obsolete">XML-файл KWallet (*.xml)</translation>
    </message>
    <message>
        <source>Add New Group...</source>
        <translation>Дадаць новую групу...</translation>
    </message>
    <message>
        <source>Edit Group...</source>
        <translation type="obsolete">Изменить группу...</translation>
    </message>
    <message>
        <source>Delete Group</source>
        <translation type="obsolete">Удалить группу</translation>
    </message>
    <message>
        <source>Copy Password to Clipboard</source>
        <translation type="obsolete">Скопировать &amp;пароль в буфер обмена</translation>
    </message>
    <message>
        <source>Copy Username to Clipboard</source>
        <translation type="obsolete">Скопировать имя в буфер обмена</translation>
    </message>
    <message>
        <source>Open URL</source>
        <translation type="obsolete">Открыть &amp;ссылку</translation>
    </message>
    <message>
        <source>Save Attachment As...</source>
        <translation type="obsolete">Сохранить вложение как...</translation>
    </message>
    <message>
        <source>Add New Entry...</source>
        <translation type="obsolete">Добавить новую запись...</translation>
    </message>
    <message>
        <source>View/Edit Entry...</source>
        <translation type="obsolete">Просмотр/правка записи...</translation>
    </message>
    <message>
        <source>Delete Entry</source>
        <translation type="obsolete">Удалить запись</translation>
    </message>
    <message>
        <source>Clone Entry</source>
        <translation type="obsolete">Дублировать запись</translation>
    </message>
    <message>
        <source>Search In Database...</source>
        <translation type="obsolete">Поиск в базе паролей...</translation>
    </message>
    <message>
        <source>Search in this group...</source>
        <translation type="obsolete">Поиск в текущей группе...</translation>
    </message>
    <message>
        <source>Show Toolbar</source>
        <translation type="obsolete">Отобразить панель инструментов</translation>
    </message>
    <message>
        <source>Show Entry Details</source>
        <translation type="obsolete">Отобразить данные записи</translation>
    </message>
    <message>
        <source>Hide Usernames</source>
        <translation type="obsolete">Скрыть имена</translation>
    </message>
    <message>
        <source>Hide Passwords</source>
        <translation type="obsolete">Скрыть пароли</translation>
    </message>
    <message>
        <source>Title</source>
        <translation type="obsolete">Название</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="obsolete">Имя</translation>
    </message>
    <message>
        <source>URL</source>
        <translation type="obsolete">Ссылка</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">Пароль</translation>
    </message>
    <message>
        <source>Comment</source>
        <translation type="obsolete">Комментарий</translation>
    </message>
    <message>
        <source>Expires</source>
        <translation type="obsolete">Окончание</translation>
    </message>
    <message>
        <source>Creation</source>
        <translation type="obsolete">Создание</translation>
    </message>
    <message>
        <source>Last Change</source>
        <translation type="obsolete">Последнее изменение</translation>
    </message>
    <message>
        <source>Last Access</source>
        <translation type="obsolete">Последний доступ</translation>
    </message>
    <message>
        <source>Attachment</source>
        <translation type="obsolete">Вложение</translation>
    </message>
    <message>
        <source>Show Statusbar</source>
        <translation type="obsolete">Отобразить панель статуса</translation>
    </message>
    <message>
        <source>Plain Text (*.txt)</source>
        <translation type="obsolete">Plain Text (*.txt)</translation>
    </message>
    <message>
        <source>Hide</source>
        <translation>Схаваць</translation>
    </message>
    <message>
        <source>Perform AutoType</source>
        <translation type="obsolete">Применить автоввод</translation>
    </message>
    <message>
        <source>Type Here</source>
        <translation type="obsolete">Ввести сюда</translation>
    </message>
    <message>
        <source>Toolbar Icon Size</source>
        <translation type="obsolete">Размер значков панели инструментов</translation>
    </message>
    <message>
        <source>16x16</source>
        <translation type="obsolete">16x16</translation>
    </message>
    <message>
        <source>22x22</source>
        <translation type="obsolete">22x22</translation>
    </message>
    <message>
        <source>28x28</source>
        <translation type="obsolete">28x28</translation>
    </message>
    <message>
        <source>&amp;View</source>
        <translation>&amp;Выгляд</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <source>&amp;Import from...</source>
        <translation>&amp;Імпартаваць з...</translation>
    </message>
    <message>
        <source>&amp;Export to...</source>
        <translation>&amp;Экспартаваць у...</translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation type="obsolete">&amp;Правка</translation>
    </message>
    <message>
        <source>E&amp;xtras</source>
        <translation>Д&amp;адаткова</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Даведка</translation>
    </message>
    <message>
        <source>&amp;New Database...</source>
        <translation>&amp;Стварыць базу пароляў...</translation>
    </message>
    <message>
        <source>&amp;Open Database...</source>
        <translation>&amp;Адчыніць базу пароляў...</translation>
    </message>
    <message>
        <source>&amp;Close Database</source>
        <translation>&amp;Зачыніць базу пароляў</translation>
    </message>
    <message>
        <source>&amp;Save Database</source>
        <translation>&amp;Захаваць базу пароляў</translation>
    </message>
    <message>
        <source>Save Database &amp;As...</source>
        <translation>Захаваць базу пароляў &amp;як...</translation>
    </message>
    <message>
        <source>&amp;Database Settings...</source>
        <translation>Н&amp;аладкі базы пароляў...</translation>
    </message>
    <message>
        <source>Change &amp;Master Key...</source>
        <translation>&amp;Зьмяніць асноўны пароль...</translation>
    </message>
    <message>
        <source>E&amp;xit</source>
        <translation type="obsolete">В&amp;ыход</translation>
    </message>
    <message>
        <source>&amp;Settings...</source>
        <translation>&amp;Наладка</translation>
    </message>
    <message>
        <source>&amp;About...</source>
        <translation>&amp;Аб праграме...</translation>
    </message>
    <message>
        <source>&amp;KeePassX Handbook...</source>
        <translation>&amp;Даведка па &quot;KeePassX&quot;...</translation>
    </message>
    <message>
        <source>Recycle Bin...</source>
        <translation>Сьметніца...</translation>
    </message>
    <message>
        <source>Groups</source>
        <translation>Групы</translation>
    </message>
    <message>
        <source>&amp;Lock Workspace</source>
        <translation>&amp;Заблякаваць галоўнае акно</translation>
    </message>
    <message>
        <source>&amp;Bookmarks</source>
        <translation>&amp;Закладкі</translation>
    </message>
    <message>
        <source>Toolbar &amp;Icon Size</source>
        <translation>Па&amp;мер значкоў панэлі інструмэнтаў</translation>
    </message>
    <message>
        <source>&amp;Columns</source>
        <translation>&amp;Слупкі</translation>
    </message>
    <message>
        <source>&amp;Manage Bookmarks...</source>
        <translation>&amp;Наладзіць закладкі...</translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation>В&amp;ыхад</translation>
    </message>
    <message>
        <source>&amp;Edit Group...</source>
        <translation>Зьмяніць &amp;групу...</translation>
    </message>
    <message>
        <source>&amp;Delete Group</source>
        <translation>Вы&amp;даліць групу</translation>
    </message>
    <message>
        <source>Copy Password &amp;to Clipboard</source>
        <translation>Скапіяваць &amp;пароль у буфэр абмену</translation>
    </message>
    <message>
        <source>Copy &amp;Username to Clipboard</source>
        <translation>Скапіяваць &amp;імя ў буфэр абмена</translation>
    </message>
    <message>
        <source>&amp;Open URL</source>
        <translation>Адчыніць &amp;спасылку</translation>
    </message>
    <message>
        <source>&amp;Save Attachment As...</source>
        <translation>&amp;Захаваць прычэпак як...</translation>
    </message>
    <message>
        <source>Add &amp;New Entry...</source>
        <translation>Дадаць новы &amp;запіс...</translation>
    </message>
    <message>
        <source>&amp;View/Edit Entry...</source>
        <translation>&amp;Агляд/рэдагаваньне запісу...</translation>
    </message>
    <message>
        <source>De&amp;lete Entry</source>
        <translation>&amp;Выдаліць запіс</translation>
    </message>
    <message>
        <source>&amp;Clone Entry</source>
        <translation>&amp;Дубляваць запіс</translation>
    </message>
    <message>
        <source>Search &amp;in Database...</source>
        <translation>По&amp;шук у базе пароляў...</translation>
    </message>
    <message>
        <source>Search in this &amp;Group...</source>
        <translation>Пошук у бягучай г&amp;рупе...</translation>
    </message>
    <message>
        <source>Show &amp;Entry Details</source>
        <translation>Пазакаць дадзеныя &amp;запісу</translation>
    </message>
    <message>
        <source>Hide &amp;Usernames</source>
        <translation>Схаваць &amp;імёны</translation>
    </message>
    <message>
        <source>Hide &amp;Passwords</source>
        <translation>Схаваць &amp;паролі</translation>
    </message>
    <message>
        <source>&amp;Title</source>
        <translation>&amp;Назва</translation>
    </message>
    <message>
        <source>User&amp;name</source>
        <translation>Імя &amp;карыстальніка</translation>
    </message>
    <message>
        <source>&amp;URL</source>
        <translation>&amp;Спасылка</translation>
    </message>
    <message>
        <source>&amp;Password</source>
        <translation>&amp;Пароль</translation>
    </message>
    <message>
        <source>&amp;Comment</source>
        <translation>&amp;Камэнтар</translation>
    </message>
    <message>
        <source>E&amp;xpires</source>
        <translation>&amp;Заканчэньне тэрміну прыдатнасьці</translation>
    </message>
    <message>
        <source>C&amp;reation</source>
        <translation>&amp;Стварэньне</translation>
    </message>
    <message>
        <source>&amp;Last Change</source>
        <translation>Апошняя &amp;зьмена</translation>
    </message>
    <message>
        <source>Last &amp;Access</source>
        <translation>Апошні &amp;доступ</translation>
    </message>
    <message>
        <source>A&amp;ttachment</source>
        <translation>&amp;Прычэпак</translation>
    </message>
    <message>
        <source>Show &amp;Statusbar</source>
        <translation>Паказаць &amp;панэль статусу</translation>
    </message>
    <message>
        <source>&amp;Perform AutoType</source>
        <translation>Ужыць &amp;аўтаўвод</translation>
    </message>
    <message>
        <source>&amp;16x16</source>
        <translation>&amp;16x16</translation>
    </message>
    <message>
        <source>&amp;22x22</source>
        <translation>&amp;22x22</translation>
    </message>
    <message>
        <source>2&amp;8x28</source>
        <translation>2&amp;8x28</translation>
    </message>
    <message>
        <source>&amp;Password Generator...</source>
        <translation>&amp;Генэратар пароляў...</translation>
    </message>
    <message>
        <source>&amp;Group (search results only)</source>
        <translation type="obsolete">&amp;Группа (поиск в результатах предыдущего поиска)</translation>
    </message>
    <message>
        <source>Show &amp;Expired Entries...</source>
        <translation>Паказаць запісы з &amp;скончаным тэрмінам прыдатнасьці...</translation>
    </message>
    <message>
        <source>&amp;Add Bookmark...</source>
        <translation>&amp;Дадаць закладку...</translation>
    </message>
    <message>
        <source>Bookmark &amp;this Database...</source>
        <translation>&amp;Дадаць гэтую базу пароляў у закладкі...</translation>
    </message>
    <message>
        <source>&amp;Add New Subgroup...</source>
        <translation>&amp;Дадаць новую падгрупу...</translation>
    </message>
    <message>
        <source>Copy URL to Clipboard</source>
        <translation>Скапіяваць спасылку ў буфэр абмену</translation>
    </message>
    <message>
        <source>&amp;Entries</source>
        <translation>&amp;Запісы</translation>
    </message>
    <message>
        <source>&amp;Groups</source>
        <translation>&amp;Групы</translation>
    </message>
    <message>
        <source>Sort groups</source>
        <translation>Упарадкаваць групы</translation>
    </message>
    <message>
        <source>S&amp;ynchronize Database...</source>
        <translation type="obsolete">&amp;Синхронизировать базу паролей...</translation>
    </message>
    <message>
        <source>&amp;Group</source>
        <translation>&amp;Група</translation>
    </message>
    <message>
        <source>&amp;Minimize Window</source>
        <translation>&amp;Мінізаваць акно</translation>
    </message>
</context>
<context>
    <name>ManageBookmarksDlg</name>
    <message>
        <source>Manage Bookmarks</source>
        <translation>Наладзіць закладкі</translation>
    </message>
</context>
<context>
    <name>PasswordDialog</name>
    <message>
        <source>Enter Master Key</source>
        <translation>Увядзіце галоўны пароль</translation>
    </message>
    <message>
        <source>Set Master Key</source>
        <translation>Вызначэньне галоўнага пароля</translation>
    </message>
    <message>
        <source>Change Master Key</source>
        <translation>Зьмена асноўнага пароля</translation>
    </message>
    <message>
        <source>Database Key</source>
        <translation>Ключ базы пароляў</translation>
    </message>
    <message>
        <source>Last File</source>
        <translation>Апошні файл</translation>
    </message>
    <message>
        <source>Select a Key File</source>
        <translation>Выбар файл-ключа</translation>
    </message>
    <message>
        <source>All Files (*)</source>
        <translation>Усе файлы (*)</translation>
    </message>
    <message>
        <source>Key Files (*.key)</source>
        <translation>Key-файлы (*.key)</translation>
    </message>
    <message>
        <source>Please enter a Password or select a key file.</source>
        <translation>Увядзіце пароль ці абярыце файл-ключ.</translation>
    </message>
    <message>
        <source>Please enter a Password.</source>
        <translation>Увядзіце пароль.</translation>
    </message>
    <message>
        <source>Please provide a key file.</source>
        <translation>Абярыце файл-ключ.</translation>
    </message>
    <message>
        <source>%1:
No such file or directory.</source>
        <translation>%1:
Файл ці тэчка ня знойдзены.</translation>
    </message>
    <message>
        <source>The selected key file or directory is not readable.</source>
        <translation>Абраны файл ці тэчка не даступны.</translation>
    </message>
    <message>
        <source>The given directory does not contain any key files.</source>
        <translation>Абраная тэчка не зьмяшчае файлы-ключоў.</translation>
    </message>
    <message>
        <source>The given directory contains more then one key files.
Please specify the key file directly.</source>
        <translation>Абраная тэчка зьмяшчае больш за адзін файл-ключ.
Калі ласка, абярыце канкрэтны файл-ключ.</translation>
    </message>
    <message>
        <source>%1:
File is not readable.</source>
        <translation>%1:
Файл не даступны.</translation>
    </message>
    <message>
        <source>Create Key File...</source>
        <translation>Стварыць файл-ключ...</translation>
    </message>
</context>
<context>
    <name>PasswordDlg</name>
    <message>
        <source>TextLabel</source>
        <translation type="obsolete">ТекстЛабел</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="obsolete">...</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
    <message>
        <source>Enter a Password and/or choose a key file.</source>
        <translation>Увядзіце пароль і/ці абярыце файл-ключ.</translation>
    </message>
    <message>
        <source>Key</source>
        <translation>Ключ</translation>
    </message>
    <message>
        <source>Password:</source>
        <translation>Пароль:</translation>
    </message>
    <message>
        <source>Key file or directory:</source>
        <translation type="obsolete">Папка с файл-ключом:</translation>
    </message>
    <message>
        <source>&amp;Browse...</source>
        <translation>&amp;Агляд...</translation>
    </message>
    <message>
        <source>Alt+B</source>
        <translation type="obsolete">Alt+B</translation>
    </message>
    <message>
        <source>Use Password AND Key File</source>
        <translation type="obsolete">Использовать пароль И файл-ключ</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="obsolete">Выход</translation>
    </message>
    <message>
        <source>Password Repet.:</source>
        <translation type="obsolete">Повтор пароля:</translation>
    </message>
    <message>
        <source>Last File</source>
        <translation>Апошні файл</translation>
    </message>
    <message>
        <source>Key File:</source>
        <translation>Файл-ключ:</translation>
    </message>
    <message>
        <source>Generate Key File...</source>
        <translation>Стварыць файл-ключ...</translation>
    </message>
    <message>
        <source>Please repeat your password:</source>
        <translation>Увядзіце паўторна пароль:</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>Назад</translation>
    </message>
    <message>
        <source>Passwords are not equal.</source>
        <translation>Паролі не супадаюць.</translation>
    </message>
</context>
<context>
    <name>PwDatabase</name>
    <message>
        <source>Unknown Error</source>
        <translation type="obsolete">Неизвестная ошибка</translation>
    </message>
    <message>
        <source>Wrong Signature</source>
        <translation type="obsolete">Неверная сигнатура</translation>
    </message>
    <message>
        <source>Could not open key file.</source>
        <translation type="obsolete">Невозможно открыть файл-ключ.</translation>
    </message>
    <message>
        <source>Key file could not be written.</source>
        <translation type="obsolete">Файл-ключ не записываем.</translation>
    </message>
    <message>
        <source>Could not open file.</source>
        <translation type="obsolete">Невозможно открыть файл.</translation>
    </message>
    <message>
        <source>Could not open file for writing.</source>
        <translation type="obsolete">Невозможно открыть файл для записи.</translation>
    </message>
    <message>
        <source>Unsupported File Version.</source>
        <translation type="obsolete">Неподдерживаемая версия файла.</translation>
    </message>
    <message>
        <source>Unknown Encryption Algorithm.</source>
        <translation type="obsolete">Неизвестный алгоритм шифрования.</translation>
    </message>
    <message>
        <source>Decryption failed.
The key is wrong or the file is damaged.</source>
        <translation type="obsolete">Расшифровка прервана.
Ключ неверен или файл повреждён.</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Warning</source>
        <translation type="obsolete">Внимание</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation type="obsolete">Файл &apos;%1&apos; не найден.</translation>
    </message>
    <message>
        <source>File not found.</source>
        <translation type="obsolete">Файл не найден.</translation>
    </message>
    <message>
        <source>Could not open file.</source>
        <translation type="obsolete">Невозможно открыть файл.</translation>
    </message>
    <message>
        <source>Unsupported file version.</source>
        <translation type="obsolete">Неподдерживаемая версия файла.</translation>
    </message>
    <message>
        <source>Unsupported hash algorithm.</source>
        <translation type="obsolete">Неподдерживаемы хэш алгоритм.</translation>
    </message>
    <message>
        <source>Unsupported encryption algorithm.</source>
        <translation type="obsolete">Неизвестный алгоритм шифрования.</translation>
    </message>
    <message>
        <source>Wrong password.</source>
        <translation type="obsolete">Неверный пароль.</translation>
    </message>
    <message>
        <source>File is empty.</source>
        <translation type="obsolete">Файл пуст.</translation>
    </message>
    <message>
        <source>Invalid XML file.</source>
        <translation type="obsolete">Неверный файл XML.</translation>
    </message>
    <message>
        <source>Document does not contain data.</source>
        <translation type="obsolete">Документ не содержит данных.</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Ошибка</translation>
    </message>
    <message>
        <source>Warning:</source>
        <translation type="obsolete">Внимание:</translation>
    </message>
    <message>
        <source>Invalid RGB color value.
</source>
        <translation type="obsolete">Неверное значение цвета RGB.
</translation>
    </message>
    <message>
        <source>Never</source>
        <translation type="obsolete">Никогда</translation>
    </message>
</context>
<context>
    <name>SearchDialog</name>
    <message>
        <source>Search</source>
        <translation>Пошук</translation>
    </message>
</context>
<context>
    <name>Search_Dlg</name>
    <message>
        <source>Alt+T</source>
        <translation type="obsolete">Alt+T</translation>
    </message>
    <message>
        <source>Alt+U</source>
        <translation type="obsolete">Alt+U</translation>
    </message>
    <message>
        <source>A&amp;nhang</source>
        <translation>&amp;Укладаньне</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <source>Alt+W</source>
        <translation type="obsolete">Alt+W</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
    <message>
        <source>Search...</source>
        <translation>Пошук...</translation>
    </message>
    <message>
        <source>Search For:</source>
        <translation>Пошук для:</translation>
    </message>
    <message>
        <source>Regular E&amp;xpression</source>
        <translation>Рэгулярны &amp;выраз</translation>
    </message>
    <message>
        <source>Alt+X</source>
        <translation type="obsolete">Alt+X</translation>
    </message>
    <message>
        <source>&amp;Case Sensitive</source>
        <translation>&amp;З улікам рэгістру</translation>
    </message>
    <message>
        <source>Include:</source>
        <translation>Уключае:</translation>
    </message>
    <message>
        <source>&amp;Titles</source>
        <translation>&amp;Назва</translation>
    </message>
    <message>
        <source>&amp;Usernames</source>
        <translation>&amp;Імя карыстальніка</translation>
    </message>
    <message>
        <source>C&amp;omments</source>
        <translation>Ка&amp;мэнтар</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation type="obsolete">Alt+O</translation>
    </message>
    <message>
        <source>U&amp;RLs</source>
        <translation>&amp;Спасылкі</translation>
    </message>
    <message>
        <source>Alt+R</source>
        <translation type="obsolete">Alt+R</translation>
    </message>
    <message>
        <source>Pass&amp;words</source>
        <translation>&amp;Паролі</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="obsolete">Поиск</translation>
    </message>
    <message>
        <source>Clo&amp;se</source>
        <translation type="obsolete">&amp;Закрыть</translation>
    </message>
    <message>
        <source>Alt+S</source>
        <translation type="obsolete">Alt+S</translation>
    </message>
    <message>
        <source>Include Subgroups (recursive)</source>
        <translation>Уключая падгрупы (рэкурсіўна)</translation>
    </message>
    <message>
        <source>Attachment</source>
        <translation type="obsolete">Вложение</translation>
    </message>
</context>
<context>
    <name>SelectIconDlg</name>
    <message>
        <source>Icon Selection</source>
        <translation>Выбар значка</translation>
    </message>
    <message>
        <source>Add Custom Icon...</source>
        <translation type="obsolete">Добавить свой значок...</translation>
    </message>
    <message>
        <source>Pick</source>
        <translation type="obsolete">Выбрать</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <source>O&amp;K</source>
        <translation type="obsolete">O&amp;K</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation type="obsolete">Alt+K</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Наладкі</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Отмена</translation>
    </message>
    <message>
        <source>Clear clipboard after:</source>
        <translation>Чысьціць буфэр абмену праз:</translation>
    </message>
    <message>
        <source>Seconds</source>
        <translation type="obsolete">секунд</translation>
    </message>
    <message>
        <source>Sh&amp;ow passwords in plain text by default</source>
        <translation type="obsolete">&amp;Отображать пароли в текстовом виде по умолчанию</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation type="obsolete">Alt+O</translation>
    </message>
    <message>
        <source>Appea&amp;rance</source>
        <translation type="obsolete">&amp;Внешний вид</translation>
    </message>
    <message>
        <source>Banner Color</source>
        <translation>Колер банэра</translation>
    </message>
    <message>
        <source>Text Color:</source>
        <translation>Колер тэксту:</translation>
    </message>
    <message>
        <source>Change...</source>
        <translation>Зьмяніць...</translation>
    </message>
    <message>
        <source>Color 2:</source>
        <translation>Колер 2:</translation>
    </message>
    <message>
        <source>C&amp;hange...</source>
        <translation>&amp;Зьмяніць</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation type="obsolete">Alt+H</translation>
    </message>
    <message>
        <source>Color 1:</source>
        <translation>Колер 1:</translation>
    </message>
    <message>
        <source>Expand group tree when opening a database</source>
        <translation type="obsolete">Раскрывать дерево групп при открытии базы паролей</translation>
    </message>
    <message>
        <source>&amp;Other</source>
        <translation type="obsolete">&amp;Другой</translation>
    </message>
    <message>
        <source>Browser Command:</source>
        <translation type="obsolete">Команда браузера:</translation>
    </message>
    <message>
        <source>Securi&amp;ty</source>
        <translation type="obsolete">Безопасность</translation>
    </message>
    <message>
        <source>Alternating Row Colors</source>
        <translation>Зьменныя колеры слупкоў</translation>
    </message>
    <message>
        <source>Browse...</source>
        <translation>Агляд...</translation>
    </message>
    <message>
        <source>Remember last key type and location</source>
        <translation>Запамінаць апошні тып і становішча ключа</translation>
    </message>
    <message>
        <source>Mounting Root:</source>
        <translation type="obsolete">Корень монтирования:</translation>
    </message>
    <message>
        <source>Remember last opened file</source>
        <translation>Запамінаць апошні адчынены файл</translation>
    </message>
    <message>
        <source>Show system tray icon</source>
        <translation>Паказываць іконку ў сыстэмным трэі </translation>
    </message>
    <message>
        <source>Minimize to tray when clicking the main window&apos;s close button</source>
        <translation>Згортвацца ў сыстэмны трэй пры закрыцьці галоўнага акна  </translation>
    </message>
    <message utf8="true">
        <source>Alt+Ö</source>
        <translation type="obsolete">Alt+O</translation>
    </message>
    <message>
        <source>Save recent directories of file dialogs</source>
        <translation>Захоўваць апошнія абраныя тэчкі дыялёгаў выбару файла</translation>
    </message>
    <message>
        <source>Group tree at start-up:</source>
        <translation>Групоўка дрэва пры запуску праграмы:</translation>
    </message>
    <message>
        <source>Restore last state</source>
        <translation>Аднаўляць апошняе стан</translation>
    </message>
    <message>
        <source>Expand all items</source>
        <translation>Разгарнуць ўсё дрэва</translation>
    </message>
    <message>
        <source>Do not expand any item</source>
        <translation>Не разгортваць дрэва</translation>
    </message>
    <message>
        <source>Security</source>
        <translation>Бясьпека</translation>
    </message>
    <message>
        <source>Edit Entry Dialog</source>
        <translation>Дыялёг рэдагаваньня запісаў</translation>
    </message>
    <message>
        <source>Plug-Ins</source>
        <translation>Пашырэньні (Plug-Ins)</translation>
    </message>
    <message>
        <source>None</source>
        <translation>Нічога</translation>
    </message>
    <message>
        <source>Gnome Desktop Integration (Gtk 2.x)</source>
        <translation>Gnome інтэграцыя (Gtk 2.x)</translation>
    </message>
    <message>
        <source>KDE 4 Desktop Integration</source>
        <translation>KDE 4 інтэграцыя</translation>
    </message>
    <message>
        <source>You need to restart the program before the changes take effect.</source>
        <translation>Зьмены ўступяць ў сілу пасьля перазапуску праграмы.</translation>
    </message>
    <message>
        <source>Configure...</source>
        <translation>Канфігурацыя...</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation>Дадаткова</translation>
    </message>
    <message>
        <source>Clear History Now</source>
        <translation>Ачысьціць гісторыю зараз</translation>
    </message>
    <message>
        <source>Always ask before deleting entries or groups</source>
        <translation>Перапытваць перад выдаленьнем запісаў і групаў</translation>
    </message>
    <message>
        <source>Customize Entry Detail View...</source>
        <translation>Наладка акна дэтальнай інфармацыі запісу....</translation>
    </message>
    <message>
        <source>You can disable several features of KeePassX here according to your needs in order to keep the user interface slim.</source>
        <translation>Для спрашчэньня інтэрфэйсу карыстальніка Вы можаце адключыць тут некаторыя функцыі KeePassX.</translation>
    </message>
    <message>
        <source>Bookmarks</source>
        <translation>Закладкі</translation>
    </message>
    <message>
        <source>Auto-Type Fine Tuning</source>
        <translation>Наладка функцыі аўтаўводу</translation>
    </message>
    <message>
        <source>Time between the activation of an auto-type action by the user and the first simulated key stroke.</source>
        <translation>Затрымка перад пачаткам сымуляцыі націсканьня клявішаў</translation>
    </message>
    <message>
        <source>ms</source>
        <translation>мс</translation>
    </message>
    <message>
        <source>Pre-Gap:</source>
        <translation>Pre-Gap:</translation>
    </message>
    <message>
        <source>Key Stroke Delay:</source>
        <translation>Час націску клявішы</translation>
    </message>
    <message>
        <source>Delay between two simulated key strokes. Increase this if Auto-Type is randomly skipping characters.</source>
        <translation>Затрымкі паміж націскам клявішаў</translation>
    </message>
    <message>
        <source>The directory where storage devices like CDs and memory sticks are normally mounted.</source>
        <translation>Тэчка мантаваньня прыладаў (CD/USB-накапляльнікаў).</translation>
    </message>
    <message>
        <source>Media Root:</source>
        <translation>Мэдыя Root:</translation>
    </message>
    <message>
        <source>Enable this if you want to use your bookmarks and the last opened file independet from their absolute paths. This is especially useful when using KeePassX portably and therefore with changing mount points in the file system.</source>
        <translation>Не выкарыстоўваць абсалютныя шляхі для закладак і апошняга адкрытага файла. Гэта найбольш зручна пры карыстаньні перанасной вэрсіі KeePassX.</translation>
    </message>
    <message>
        <source>Save relative paths (bookmarks and last file)</source>
        <translation>Захоўваць адносныя шляхі (для закладак і апошняга адкрытага файла)</translation>
    </message>
    <message>
        <source>Minimize to tray instead of taskbar</source>
        <translation>Мінізаваць у сыстэмны трэй</translation>
    </message>
    <message>
        <source>Start minimized</source>
        <translation>Мінізаваць пры загрузцы</translation>
    </message>
    <message>
        <source>Start locked</source>
        <translation>Блякаваць галоўнае акно пры загрузцы</translation>
    </message>
    <message>
        <source>Lock workspace when minimizing the main window</source>
        <translation>Блякаваць галоўнае акно пры мінізацыі праграмы</translation>
    </message>
    <message>
        <source>Global Auto-Type Shortcut:</source>
        <translation>Глябальная гарачая клявіша для аўтаўвода</translation>
    </message>
    <message>
        <source>Custom Browser Command</source>
        <translation>Каманда браўзэра</translation>
    </message>
    <message>
        <source>Browse</source>
        <translation>Агляд</translation>
    </message>
    <message>
        <source>Automatically save database on exit and workspace locking</source>
        <translation>Аўтаматычна захоўваць базу пароляў пры закрыцьці праграмы, ці блякаваньні галоўнага акна</translation>
    </message>
    <message>
        <source>Show plain text passwords in:</source>
        <translation>Адлюстроўваць паролі ў:</translation>
    </message>
    <message>
        <source>Database Key Dialog</source>
        <translation>Дыялёг базы дадзеных ключоў</translation>
    </message>
    <message>
        <source>seconds</source>
        <translation>Сэкунд</translation>
    </message>
    <message>
        <source>Lock database after inactivity of</source>
        <translation>Блякаваць базу дадзеных у выпадку невыкарыстаньня кампутара ў плыні</translation>
    </message>
    <message>
        <source>Use entries&apos; title to match the window for Global Auto-Type</source>
        <translation>Выкарыстоўваць назвы запісаў пры пошуку акна для аўтаўвода</translation>
    </message>
    <message>
        <source>General (1)</source>
        <translation>Асноўныя (1)</translation>
    </message>
    <message>
        <source>General (2)</source>
        <translation>Асноўныя (2)</translation>
    </message>
    <message>
        <source>Appearance</source>
        <translation>Вонкавы выгляд</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Мова</translation>
    </message>
    <message>
        <source>Save backups of modified entries into the &apos;Backup&apos; group</source>
        <translation>Пры зьмене запісаў захоўваць копію ў групу &apos;Backup&apos;</translation>
    </message>
    <message>
        <source>Delete backup entries older than:</source>
        <translation>Выдаляць копіі запісаў праз:</translation>
    </message>
    <message>
        <source>days</source>
        <translation>дзён</translation>
    </message>
    <message>
        <source>Automatically save database after every change</source>
        <translation>Аўтаматычна захоўваць базу пароляў пры кожнай зьмене</translation>
    </message>
    <message>
        <source>System Language</source>
        <translation type="obsolete">Язык системы</translation>
    </message>
    <message>
        <source>English</source>
        <translation type="obsolete">Английский</translation>
    </message>
    <message>
        <source>Language:</source>
        <translation>Мова:</translation>
    </message>
    <message>
        <source>Author:</source>
        <translation>Аўтар:</translation>
    </message>
    <message>
        <source>Show window always on top</source>
        <translation>Паказваць па-над усімі вокнамі.</translation>
    </message>
</context>
<context>
    <name>ShortcutWidget</name>
    <message>
        <source>Ctrl</source>
        <translation>Ctrl</translation>
    </message>
    <message>
        <source>Shift</source>
        <translation>Shift</translation>
    </message>
    <message>
        <source>Alt</source>
        <translation>Alt</translation>
    </message>
    <message>
        <source>AltGr</source>
        <translation>AltGr</translation>
    </message>
    <message>
        <source>Win</source>
        <translation>Win</translation>
    </message>
</context>
<context>
    <name>SimplePasswordDialog</name>
    <message>
        <source>O&amp;K</source>
        <translation type="obsolete">O&amp;K</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation type="obsolete">Alt+K</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="obsolete">...</translation>
    </message>
    <message>
        <source>Enter your Password</source>
        <translation>Увядзіце пароль::</translation>
    </message>
    <message>
        <source>Password:</source>
        <translation>Пароль:</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Отмена</translation>
    </message>
</context>
<context>
    <name>StandardDatabase</name>
    <message>
        <source>Could not open file.</source>
        <translation type="obsolete">Невозможно открыть файл.</translation>
    </message>
    <message>
        <source>Wrong Signature</source>
        <translation type="obsolete">Неверная сигнатура</translation>
    </message>
    <message>
        <source>Unsupported File Version.</source>
        <translation type="obsolete">Неподдерживаемая версия файла.</translation>
    </message>
    <message>
        <source>Unknown Encryption Algorithm.</source>
        <translation type="obsolete">Неизвестный алгоритм шифрования.</translation>
    </message>
    <message>
        <source>Decryption failed.
The key is wrong or the file is damaged.</source>
        <translation type="obsolete">Расшифровка прервана.
Ключ неверен или файл повреждён.</translation>
    </message>
    <message>
        <source>Could not open file for writing.</source>
        <translation type="obsolete">Невозможно открыть файл для записи.</translation>
    </message>
</context>
<context>
    <name>TargetWindowDlg</name>
    <message>
        <source>Auto-Type: Select Target Window</source>
        <translation>Аўтаўвод: абярыце акно для ўводу</translation>
    </message>
    <message>
        <source>To specify the target window, either select an existing currently-opened window
from the drop-down list, or enter the window title manually:</source>
        <translation>Абярыце акно для ўводу са сьпісу, ці ўвядзіце імя акна:</translation>
    </message>
</context>
<context>
    <name>Translation</name>
    <message>
        <source>$TRANSLATION_AUTHOR</source>
        <translation>Сакалоў Аляксей</translation>
    </message>
    <message>
        <source>$TRANSLATION_AUTHOR_EMAIL</source>
        <comment>Here you can enter your email or homepage if you want.</comment>
        <translation>nullbsd@gmail.com</translation>
    </message>
    <message>
        <source>$LANGUAGE_NAME</source>
        <comment>Insert your language name in the format: English (United States)</comment>
        <translation>Беларуская (Беларусь)</translation>
    </message>
</context>
<context>
    <name>TrashCanDialog</name>
    <message>
        <source>Title</source>
        <translation type="obsolete">Название</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="obsolete">Имя</translation>
    </message>
</context>
<context>
    <name>WorkspaceLockedWidget</name>
    <message>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;The workspace is locked.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Галоўнае акно заблякавана.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Unlock</source>
        <translation>Разблякаваць</translation>
    </message>
    <message>
        <source>Close Database</source>
        <translation>Закрыць базу пароляў</translation>
    </message>
</context>
<context>
    <name>dbsettingdlg_base</name>
    <message>
        <source>Database Settings</source>
        <translation type="obsolete">Н&amp;астройки базы паролей...</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation type="obsolete">Шифрование</translation>
    </message>
    <message>
        <source>Algorithm:</source>
        <translation type="obsolete">Алгоритм:</translation>
    </message>
    <message>
        <source>?</source>
        <translation type="obsolete">?</translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation type="obsolete">O&amp;K</translation>
    </message>
    <message>
        <source>Ctrl+K</source>
        <translation type="obsolete">Ctrl+K</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Отмена</translation>
    </message>
    <message>
        <source>Ctrl+C</source>
        <translation type="obsolete">Ctrl+C</translation>
    </message>
</context>
</TS>
